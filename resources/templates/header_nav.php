<link rel="stylesheet" type="text/css" href="css/menu.css" />
<script src="js/menu.js"></script>

<base target="main">
<div id="navMenu" >
	<ul id="mainMenu">
	    <li><a href="/">Home</a></li>
	    <li>
	    	<a href="StudentsAll.php" onmouseover="openMenu('s2')" onmouseout="closeTime()">Students</a>
	    </li>
		<li>
			<a href="TextbookAll.php" onmouseover="openMenu('s3')" onmouseout="closeTime()">Textbooks</a>
	    </li>
	    <li>
	    	<a href="#" onmouseover="openMenu('s4')" onmouseout="closeTime()">Import</a>
	        <div id="s4" onmouseover="cancelClose()" onmouseout="closeTime()">
	        	<a href="StudentsSynch.php">Import Students</a>
	        	<a href="TextbookSynch.php">Import Textbooks</a>
	        </div>
	    </li>
	    <li>
	    	<a href="student_search.php" onmouseover="openMenu('s5')" onmouseout="closeTime()">Search by</a> 	       
	    </li>
	</ul>
</div>
<div style="clear:both"></div>
