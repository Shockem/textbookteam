<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Textbook Management</title>
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<link rel="stylesheet" type="text/css" href="css/layout.css" />
</head>

<body>
<div id="wrap">
<div id="header" style="text-align:center;">
	<h1>Textbook Management System (TMS)</h1>
</div>
<div id="nav">
<?php
	require_once("/var/www/html/resources/config.php");
	require_once(TEMPLATES_PATH . "/header_nav.php");
?>
</div>
