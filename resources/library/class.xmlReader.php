library
<?php
	/*
	 * Specific to St. Mary's tab dilimiter (\t) csv.
	 * - openFileHandle($_String) - Validates handle, opens file read only
	 * - closeFileHandle($_String) - Closes the file
	 * - get2dArray()- Returns a 2D array of the data (row, collum)
	 * 
	 * fgetcsv Returns: 
	 * - Indexed array for each collumn in a row. 
	 * - Blank lines are returned null
	 * - Returns NULL if an invalid handle is supplied or FALSE on other errors, including end of file.
	*/
	
	class xmlReader 
	{
		//Property declaration
		private $handle;//Name of file
		private $data;//Array of entire CSV

		
		//Returns False if handle is invalid
		function openFileHandle($_String)
		{
			if ($_tempHandle = fopen($_String, "r") !== FALSE)
			{
				$handle = $_String;
				return TRUE;
			}
			else return FALSE;
		}
		
		function closeFileHandle()
		{
			if ($handle != FALSE || $handle != NULL)
			{
					
				fclose($handle);
				$handle = NULL;
				return TRUE;
			}
			
			return FALSE;
		}
		
		function get2dArray()
		{
			if ($handle != FALSE || $handle != NULL)
			{
				$_row= 0;
				//Reads and advances pointer
				while(($_lineArray = fgetcsv($handle, 1000, "\t")) !== FALSE)
				{
					$_colTotal = count($_lineArray);
					for ($_col = 0; $col< $_colTotal; $col ++)
					{
						$data[$_row][$_col] = $_lineArray[$col];
					}
					$_row++;
				}
				return $data;
			}	
			else return ($data = NULL);
		}
	}
?>