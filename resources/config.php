<?php

/** The important thing to realize is that the config file should be included on every page **/

$config = array(
	"db" => array(
		"students" => array(
			"db" => "Students",
			"user" => "beckerk",
			"pass" => "",
			"host" => "localhost"
		),
		"test" => array(
			"db" => "Test",
			"user" => "beckerk",
			"pass" => "",
			"host" => "localhost"
		)
	),
	"urls" => array(
		"baseUrl" => "http://mogul.strose.com"
	),
	"paths" => array(
		"resources" => "/resources",
		"images" => array(
			"content" => $_SERVER["DOCUMENT_ROOT"] . "/images/content",
			"layout" => $_SERVER["DOCUMENT_ROOT"] . "/images/layout"
		)
	)
);


defined("LIBRARY_PATH")
	or define("LIBRARY_PATH", realpath(dirname(__FILE__) . '/library'));
	
defined("TEMPLATES_PATH")
	or define("TEMPLATES_PATH", realpath(dirname(__FILE__) . '/templates'));

defined("RESOURCE_PATH")
    or define("RESOURCE_PATH", realpath(dirname(__FILE__) . '/'));
	
defined("IMAGE_PATH")
    or define("IMAGE_PATH", realpath(dirname(__FILE__) . '/images'));
	
defined("CLASS_PATH")
    or define("CLASS_PATH", realpath($_SERVER["DOCUMENT_ROOT"] . '/classes/'));	
/*
	Error reporting.
*/
error_reporting(E_ALL ^ E_NOTICE);

include_once($_SERVER['DOCUMENT_ROOT'] . '/classes/mysql.class.php');
$db = new mysql($config['db']['students']);
$assignmentupdate = "INSERT INTO assignments (stdnID, bookID) 
					(SELECT students.student_id, textbooks.book_id
					FROM students
					INNER JOIN textbooks on students.grade = textbooks.grade)";
?>