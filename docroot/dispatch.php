<?php
//extract parameters
$url_pieces = explode("/",$_SERVER["PATH_INFO"]);
$action = $url_pieces[1];
$params = array();
if(count($url_pieces)>2){
	$params = array_slice($url_pieces, 2);
}

$manage_mode_file = $_SERVER["DOCUMENT_ROOT"] . "/docroot/$action.php";
 
if(file_exists("$manage_mode_file.php"))
{
   require_once("$manage_mode_file.php");
}
else
{
   require_once($_SERVER["DOCUMENT_ROOT"] . '/docroot/index.php');
}
?>