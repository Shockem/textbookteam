<?php
class Textbook {
	private $db;
	
	public function __construct($database) {
		$this->db = $database;
	}
	
	public function getTextbooks() {
		return($this->db->query('SELECT * FROM Students.textbooks'));
	}
	
	public function filterTextbooks($grade) {
		if($grade) {
			return($this->getTextbooksByGrade($grade));
		}
		return($this->getTextbooks());
	}
	
	private function getTextbooksByGrade($grade) {
		$sql = sprintf("SELECT * FROM Students.textbooks WHERE grade = %d", mysql_real_escape_string($grade));
		return($this->db->query($sql));
	}
	
	public function getTextbookInfo($id) {
		$sql = sprintf("SELECT * FROM Students.textbooks WHERE book_id = %d", mysql_real_escape_string($id));
		return($this->db->query($sql, false, false));
	}
	
	public function getTexbookFormData($comaDelimitedIdString){
		$sql = sprintf('SELECT subject, isb_number, publisher, book_name 
						FROM textbooks WHERE book_id IN (%s)', mysql_real_escape_string($comaDelimitedIdString));
		return($this->db->query($sql,FALSE,FALSE));
	}
}
?>
