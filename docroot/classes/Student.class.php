<?php
class Student {
	private $db;
	
	public function __construct($database) {
		$this->db = $database;
	}
	
	public function getDistricts() {
		return($this->db->query('SELECT distinct(district) FROM Students.students', false, false));
	}
	
	public function getStudents() {
		return($this->db->query('SELECT * FROM Students.students'));
	}
	
	public function filterStudents($grade, $district) {
		if($grade && $district) {
			return($this->getStudentsByGradeDistrict($grade, $district));
		}elseif($grade) {
			return($this->getStudentsByGrade($grade));
		}elseif($district) {
			return($this->getStudentsByDistrict($district));
		}
		return($this->getStudents());
	}
		
	public function getStudentInfo($id) {
		$sql = sprintf("SELECT * FROM Students.students WHERE student_id = %d", mysql_real_escape_string($id));
		return($this->db->query($sql, false, false));
	}
		
	private function getStudentsByGradeDistrict($grade, $district) {
		$sql = sprintf("SELECT * FROM Students.students WHERE grade = %d AND district = '%s'", mysql_real_escape_string($grade) , mysql_real_escape_string($district));
		return($this->db->query($sql));
	}
	
	private function getStudentsByGrade($grade) {
		$sql = sprintf("SELECT * FROM Students.students WHERE grade = %d", mysql_real_escape_string($grade));
		return($this->db->query($sql));
	}
		
	private function getStudentsByDistrict($district) {
		$sql = sprintf("SELECT * FROM Students.students WHERE district = '%s'", mysql_real_escape_string($district));
		return($this->db->query($sql));
	}
	
	public function getStudentsBookIDs($studentid){
		//Returns an array with all the books for $student - Array [n][1]
		$sql = sprintf("SELECT bookID FROM assignments WHERE stdnID = %s", mysql_real_escape_string($studentid));
		$studentbooklist = $this->db->query($sql, FALSE, FALSE);
		
		$bookid = "";
		$count = 0;
		foreach($studentbooklist as $row)
			{
				if($count>0)
				{$bookid=$bookid.", ";}
					
				$bookid= $bookid . implode(",", $row);
				$count++;
			}
		return $bookid;
	}	
}
?>
