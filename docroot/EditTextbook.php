<?php
require_once("/var/www/html/resources/config.php");
require_once(TEMPLATES_PATH . "/header.php");
require_once(CLASS_PATH . "/Textbook.class.php");
$textbook = new Textbook($db);
$book_id = $_GET['id'];
$textbook_info = $textbook->getTextbookInfo($book_id);
?>

<div id="container" style="height: 1000px;">
	<div id="main" >
		<form>
			<?php foreach($textbook_info[0] as $key => $t) { ?>
				<label><?php echo ucwords(str_replace("_", " ", $key)); ?></label><input type="text" value="<?php echo $t; ?>"/><br/><br/>
			<?php } ?>
		</form>
	</div>
</div>
