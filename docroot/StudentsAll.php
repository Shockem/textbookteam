<?php
	require_once("/var/www/html/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
	require_once(CLASS_PATH . "/Student.class.php");
	$students = new Student($db);
	$allStudents = $students->filterStudents($_GET['grade'], $_GET['district']);
	$districts = $students->getDistricts();
	$grades = array(0, 1, 2, 3, 4, 5, 6, 7, 8);
	$perPage = 15;
	$studentLists = array_chunk($allStudents, $perPage);
	$lastPage = count($studentLists);
	$page = 1;
?>
<div id="container">
	<div id="mainright" style="min-height:500px;">
		<div id="list-students" class="list">
		<?php
		foreach($studentLists as $key => $list) { 
			$display = $key == 0 ? '' : 'display: none;';
			?>
			<div id="list-<?php echo $key; ?>" class="student-list" style="<?php echo $display; ?>">
			<table>
				<tr>
					<th class="txtl">Name</th>
					<th class="txtl">Grade</th>
					<th class="txtl">District</th>
					<th class="txtl">Parent</th>
					<th class="txtl">DOB</th>
					<th/>
					<th/>
				</tr>
				<?php foreach($list as $key => $s) { 
					$student = $s['students'];
					$row_bg = $key % 2 ? 'odd' : 'even';
					?>
					<tr class="<?php echo $row_bg; ?>">
						<td><?php echo $student['firstname'] . '&nbsp' . $student['lastname']; ?></td>
						<td><?php echo $student['grade']; ?></td>
						<td><?php echo $student['district']; ?></td>
						<td><?php echo $student['guardian_first_name'] . '&nbsp' . $student['guardian_last_name']; ?></td>
						<td><?php echo $student['dob']; ?></td>
						<td><a target="_blank" href="/EditStudent.php?id=<?php echo $student['student_id']; ?>">Edit</a></td>
						<td><a target="_blank" href="/FormBOCES.php?student_id=<?php echo $student['student_id']; ?>">Print Form</a></td>
					</tr>
				<?php } ?>
			</table>
			</div>
		<?php
		}
		?>
		</div>
		<div id="paging">
			<a id="previous" href="#" onclick="lastPage();">&#171;&#171; Previous</a>
			<a id="last" href="#" onclick="nextPage();">Next &#0187;&#0187;</a>
		</div>
	</div>
	
	<div id="sidebarleft" style="min-height:500px;">
		<form name="input" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
			<div class="filter-container">
				<div class="select-container">
					<select class='filter' name="district">
						<option value="">Select District</option>
						<?php
						foreach($districts as $d) {
							$selected =  $_GET['district'] == $d['district'] ? 'selected="selected"' : ''; ?>
							<option <?php echo $selected; ?> value="<?php echo $d['district']; ?>"><?php echo $d['district']; ?></option>
						<?php
						}
						?>
					</select>
				</div><br/>
				<div class="select-container">
					<select class='filter' name="grade">
						<option value="">Select Grade</option>
						<?php
						foreach($grades as $g) { 
							$selected =  $_GET['grade'] === $g ? 'selected="selected"' : ''; ?>
							<option <?php echo $selected; ?> value="<?php echo $g; ?>"><?php echo $g; ?></option>
						<?php
						}
						?>
					</select>
				</div><br/>
				<input type="submit" name="submit" id="submit" value="Search"/>
			</div>
		</form>
	</div>
	<div id="footer">
		<?php
		require_once(TEMPLATES_PATH . "/footer.php");
		?>
	</div>
	
	<script type="text/javascript">
		var page = 0;
		var last = <?php echo $lastPage; ?>;
		function lastPage() {
			var currentPage = document.getElementById('list-'+page);
			if(page > 0) {
				currentPage.style.display = "none";
				page--;
				var last = document.getElementById('list-'+page);
				last.style.display = "block";
			}
			return false;
		}
		
		function nextPage() {
			if(page < last-1) { 
				var currentPage = document.getElementById('list-'+page);
				currentPage.style.display = "none";
				page++;
				var next = document.getElementById('list-'+page);
				next.style.display = "block";
			}
			return false;
		}
	</script>
</div>
</div>
</div>