<?php
	require_once("/var/www/html/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>
<div id="container">
	<div id="main" style="min-height:500px;">
		<div id="description">
			<p  align="center">
				<img src="<?php echo IMAGE_PATH; ?>/SMI.jpg" alt="logo">
			</p>
			<h3>Welcome to St. Mary's Institute's Textbook Management System (TMS)</h3>
			<p align = "left">
				The purpose of this system is to combine student and textbook information to generate
				 and report text books requirements for students in Kindergarten to the 8th Grade.
				<br/><br/>
				To do so the school is in charge of exporting the data for both textbooks and students.
				The system then combines the books and students in an assignment table based on their grade.
				From this assignment table the school is able to capture or generate:
				<ul align = "left">
					<li>Student Textbook Loan Form (student menu options) that is then printed via browser</li>
					<li>Students by Grade and School District (student menu options)</li>
					<li>Number of Books required for each grade level from each School District (text book menu options)</li>
				</ul>
			</p>
		</div>
	</div>
	<div id="sidebar" style="min-height:500px;">
		<div id="siteControls" style="padding:5px;">
			<b>Development:</b> In-progress Features
			<ul class="categories">
				<li><a href="StudentsAll.php">Show All Students</a></li>
				<li><a href="FormBOCES.php">Show Sample Form</a></li>
			</ul>
		</div>
	</div>
	<div id="footer">
		<?php
		require_once(TEMPLATES_PATH . "/footer.php");
		?>
	</div>
</div>
</div>
</div>