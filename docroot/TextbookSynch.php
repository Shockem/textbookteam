<!-- Author: Jorge -->
<?php
	require_once("/var/www/html/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>
<div id="container">
	<div id="mainsingle" style="min-height:500px;">
	<h2>Uploading and Updating the Textbook Database</h2>
	<b>Important instructions for preparing the data:</b>
	<p>
	Every time you upload the data a file will be copied into the server
	 and the student data in the MySQL database will be updated. 
	</p><p>
	All book assignments will be instantly generated and updated.
	</p><p>
	Please exercise caution as the process is instantaneous.
	 Entering student lists with different names will create a file back up on the
	 server which could then be retrieved by the server administrator.
	</p>
	<ol>
		<li>You can create your Textbook lists initial textbook list Microsoft excel format.</li>
		<li>You will want to have each row contain the textbook information in this order:
			<ul> 
			<li>Book Name</li>
			<li>Subject</li>
			<li>Grade (-1 through 8) - This is used to generate initial assignments.</li>
			<li>ISBN Number</li>
			<li>Price</li>
			<li>Year</li>
			<li>Author</li>
			</ul>
		</li>
		<li>IMPORTANT: The file should only have textbook information listed above:
			 <ul>
			 <li>Delete any empty rows or columns preceding the textbook information</li>
			 <li>Delete any additional rows like the name of each column</li>
			 </ul>
		</li>
		<li>From Excel select Save As and use the format "Text (Tab delimited) (*.txt)</li>
		<li>Keep the name simple</li>
		<li>If you want to review your export reopen this file in excel again</li>	 
	</ol>	
	</p>
	<h3>Select your file by using the "Choose File" Button</h3>
	Use the submit button when ready.<br/>
	<img src="<?php echo IMAGE_PATH; ?>/BOOK.jpg" alt="logo">
		<!-- Form enctype attribute "multipart/form-data" specifies we require binary data format (an uploaded file) -->
		<form action= "TextbookUploadInsert.php" method="post" enctype="multipart/form-data"><br/>
			<label for="file">Filename:</label>
			<!-- specifies that the input should be processed as a file -->
			<input type="file" name="file" id="file"><br/><br/>
			<input type="submit" name="submit" value="Submit 'textbook list' file for Upload">
		</form>
	</div>
	<div id="footer">
		<?php
		require_once(TEMPLATES_PATH . "/footer.php");
		?>
	</div>
</div>
</div>
</div>