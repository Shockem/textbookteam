<!-- Author: Jorge based on Kirks implementation-->
<?php
	require_once("/var/www/html/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>
<div id="container">
	<div id="mainright" style="min-height:500px;">
		<?php
			//$result = $db->query('SELECT * FROM `textbooks`');
			//var_dump($result);
			$result = $db->query('SELECT * FROM textbooks WHERE grade = 1');
		
   echo "<table border='1'>
			<tr>
				<th>Book_ID</th>
				<th>Book_Name</th>
				<th>Subject</th>
				<th>Grade</th>
				<th>ISBN</th>
				<th>Price</th>
				<th>Year</th>
				<th>Author</th>
			</tr>";
			
			while ($row = mysqli_fetch_array($result))
			{
				echo "<tr>";
				echo "<td>". $row['book_id'] . "</td> ";
				echo "<td>". $row['book_name'] . "</td> ";
				echo "<td>". $row['subject'] . "</td> ";
				echo "<td>". $row['grade'] . "</td> ";
				echo "<td>". $row['isb_number'] . "</td> ";
				echo "<td>". $row['year'] . "</td> ";
				echo "<td>". $row['author'] . "</td> ";
			}
		echo "</table>";
		
        ?> 
	</div>
	<div id="sidebarleft" style="min-height:500px;">
		<?php
		require_once(TEMPLATES_PATH . "/rightPanel.php");
		?>
	</div>
	<div id="footer">
		<?php
		require_once(TEMPLATES_PATH . "/footer.php");
		?>
	</div>
</div>
</div>
</div>