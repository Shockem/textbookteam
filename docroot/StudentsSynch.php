<!-- Author: Jorge -->
<?php
	require_once("/var/www/html/resources/config.php");
	require_once(TEMPLATES_PATH . "/header.php");
?>
<div id="container">
	<div id="mainsingle" style="min-height:500px;">
	<h2>Uploading and Updating the Student Database</h2>
	<b>Important instructions for preparing the data:</b>
	<p>
	Every time you upload the data a file will be copied into the server
	 and the student data in the MySQL database will be updated. 
	</p><p>
	All book assignments will be instantly generated and updated.
	</p><p>
	Please exercise caution as the process is instantaneous.
	 Entering student lists with different names will create a file back up on the
	 server which could then be retrieved by the server administrator.
	</p>
	<ol>
		<li>You will export your student lists from Power School in xls format.</li>
		<li>You will want to have each row contain the textbook information in this order:
			<ul> 
			<li>Student Number</li>
			<li>Last Name</li>
			<li>First Name</li>
			<li>Grade Level</li>
			<li>Date of Birth (2000-01-30)</li>
			<li>Father</li>
			<li>Mother</li>
			<li>Mailing Street</li>
			<li>Mailing City</li>
			<li>Mailing Zip</li>
			<li>Districtofresidence</li>
			<li>Home Phone</li>
			</ul>
		</li>
		<li>IMPORTANT: The file should only contain the student information listed above:
			 <ul>
			 <li>Delete any empty rows or columns preceding the student information</li>
			 <li>Delete any additional rows like the name of each column</li>
			 </ul>
		</li>
		<li>From Excel select Save As and use the format "Text (Tab delimited) (*.txt)</li>
		<li>Keep the name simple, if you want to review your export reopen this file in excel again.</li>	 
	</ol>	
	</p>
	<h3>Select your file by using the "Choose File" Button</h3>
	Use the submit button when ready.<br/>
	<img src="<?php echo IMAGE_PATH; ?>/STUDENTS.jpg" alt="logo">
		<!-- Form enctype attribute "multipart/form-data" specifies we require binary data format (an uploaded file) -->
		<form action= "StudentsUploadInsert.php" method="post" enctype="multipart/form-data"><br/>
			<label for="file">Filename:</label>
			<!-- specifies that the input should be processed as a file -->
			<input type="file" name="file" id="file"><br/><br/>
			<input type="submit" name="submit" value="Submit 'student list' file for Upload">
		</form>
	</div>
	<div id="footer">
		<?php
		require_once(TEMPLATES_PATH . "/footer.php");
		?>
	</div>
</div>
</div>
</div>