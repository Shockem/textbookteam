<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
<!DOCTYPE html>
<html>
<body>

<?php
//Doesn't show in Firefox? but it is in HTML Data
echo "<h2>Jorge's XML PHP!</h2>";

//Global Variables
$x=1;
$y=foo;
static $z=false;

function test(){
	//Accessing global var	
	global $x;
	$x++;
	//Accessing global var array
	$GLOBALS ['y'] = bar;
}
?> 
<br/>
This PHP file used to read from the file in mogul called 20130218 student export.xls and displays its contents.

<?
//Example #1 Read and print the entire contents of a CSV file
$row = 1;
if (($handle = fopen("20130218 student export.xls", "r")) !== FALSE) {
	echo "IF";
    while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
        $num = count($data);
        echo "<p> $num field(s) in line $row: <br /></p>\n";
        $row++;
        for ($c=0; $c < $num; $c++) {
            echo $data[$c] . "<br />\n";
		}
    }
    fclose($handle);
}
?>

</body>
</html>