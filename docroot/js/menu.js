var timeout	= 1;
var closetimer	= 0;
var item	= 0;

function openMenu(id)
{	
	cancelClose();
	if(item) item.style.visibility = 'hidden';
	item = document.getElementById(id);
	item.style.visibility = 'visible';

}
function close()
{
	if(item) item.style.visibility = 'hidden';
}
function closeTime()
{
	closetimer = window.setTimeout(close, timeout);
}
function cancelClose()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}
document.onclick = close(); 