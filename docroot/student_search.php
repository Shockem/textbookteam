<?php
require_once ("/var/www/html/resources/config.php");
require_once (TEMPLATES_PATH . "/header.php");
require_once (CLASS_PATH . "/mysql.class.php");
$lname = $_POST["last"];
$fname = $_POST["first"];
$init = FALSE;
if (empty($lname)) {
	$sql = sprintf("SELECT * FROM Students.students WHERE firstname = '%s'", mysql_real_escape_string($fname));
	$student = $db -> query($sql);

} elseif (empty($fname)) {
	$sql = sprintf("SELECT * FROM Students.students WHERE lastname = '%s'", mysql_real_escape_string($lname));
	$student = $db -> query($sql);
} else {
	$sql = sprintf("SELECT * FROM Students.students WHERE firstname = '%s' and lastname = '%s'", mysql_real_escape_string($fname), mysql_real_escape_string($lname));
	$student = $db -> query($sql);
}
$studentlist = $student;

/*if ($studentlist == null && !$init) {
	$init = TRUE;
	$message = 'No available record for such student';
	echo "<SCRIPT>
 alert('$message');
 </SCRIPT>";
}*/
?>
<script type="text/javascript">
	function check() {
		if (document.f1.last.value == "" && document.f1.first.value == "")
			alert("Enter student's first name or last name");
		else
			document.f1.submit();
	}
</script>
<div id="container">
	<div id="main" style="min-height:500px;">
		<h2>Search Students Database</h2>
		<div id="frm" >
		<form method="POST" name="f1" id="f1"  action="<?php echo $PHP_SELF; ?>">
			First Name:
			<input type="text" name="first"> &nbsp Last Name: <input type="text" name="last"> &nbsp &nbsp	<input type="submit" value="Submit" onClick="check()">
			<br>
		</form>
		</div>
		<div id="list-students" class="list">
		<?php
		foreach($studentlist as $key => $list) { 		
		?>
			<div id="list-<?php echo $key; ?>" class="student-list">
			<table>
				<tr>
					<tr>
					<th class="txtl">Name</th>
					<th class="txtl">Grade</th>
					<th class="txtl">District</th>
					<th class="txtl">Parent</th>
					<th class="txtl">DOB</th>
					<th/>
					<th/>
				</tr>
				</tr>
				<?php foreach($list as $s) {
					$students = $s;
					$row_bg = $key % 2 ? 'odd' : 'even';
					?>
					<tr class="<?php echo $row_bg; ?>">
						<td><?php echo $students['firstname'] . '&nbsp' . $students['lastname']; ?></td>
						<td><?php echo $students['grade']; ?></td>
						<td><?php echo $students['district']; ?></td>
						<td><?php echo $students['guardian_first_name'] . '&nbsp' . $students['guardian_last_name']; ?></td>
						<td><?php echo $students['dob']; ?></td>
						<td><a target="_blank" href="/EditStudent.php?id=<?php echo $students['student_id']; ?>">Edit</a></td>
					</tr>
				<?php } ?>
			</table>
			</div>
		<?php } ?>
		</div>
	</div>
	<div id="sidebar" style="min-height:500px;">
		<div id="siteControls" style="padding:5px;">
			<ul class="categories">
				<li><a href="StudentsAll.php">Show All Students</a></li>
				<li><a href="FormBOCES.php">Show Sample Form</a></li>
			</ul>
		</div>
	</div>
	<div id="footer">
		<?php
		require_once (TEMPLATES_PATH . "/footer.php");
		?>
	</div>
</div>
</div>
</div>